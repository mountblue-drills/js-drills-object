function defaults(obj, defaultObj) {
    for (let key in defaultObj) {
        if (defaultObj[key] && obj[key]===undefined) {  //if the key is undefined 
            obj[key] = defaultObj[key];   //we're adding a default value for the paticular key
        }
    }

    return obj;
}

module.exports = defaults;
