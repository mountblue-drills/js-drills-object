function getPairs(obj){
    let keyValuePairs=[];
    for(let key in obj){
        keyValuePairs.push([key, obj[key]])
    }
    return keyValuePairs
}

module.exports = getPairs
// const result = pairs({ name: 'Bruce Wayne', age: 36, location: 'Gotham' });
// console.log(result)
