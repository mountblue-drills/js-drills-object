function mapObject(obj, callback) {
    let result = {};

    for (let key in obj) {
        result[key] = callback(obj[key]);
    }

    return result;
}

module.exports = mapObject;
