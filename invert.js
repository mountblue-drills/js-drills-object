//this function toggles the key value pairs
function getInvertedObject(obj){
    let invertedObject ={};
    for(let key in obj){
        invertedObject[obj[key]]=key
    }
    return invertedObject
}
module.exports=getInvertedObject